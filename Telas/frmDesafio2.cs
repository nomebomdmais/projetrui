﻿using Nsf._2018.Modulo3.Logica.DB.Desafio2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio2 : Form
    {
        public frmDesafio2()
        {
            InitializeComponent();
        }

        private void btnIistar_Click(object sender, EventArgs e)
        {
            IngressoBusiness bus = new IngressoBusiness();
            List<IngressoDTO> lista = bus.Listar();

            dgvIngressos.AutoGenerateColumns = false;
            dgvIngressos.DataSource = lista;

            foreach (DataGridViewRow item in dgvIngressos.SelectedRows)
            {
                
            }
        }
    }
}
