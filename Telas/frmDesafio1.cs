﻿using Nsf._2018.Modulo3.Logica.DB.Desafio1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio1 : Form
    {
        public frmDesafio1()
        {
            InitializeComponent();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            ViagemBusiness bus = new ViagemBusiness();
            List<ViagemDTO> lista = bus.Listar();

            dgvViagem.AutoGenerateColumns = false;
            dgvViagem.DataSource = lista;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ViagemDTO dto = dgvViagem.CurrentRow.DataBoundItem as ViagemDTO;           
           
            TimeSpan SomaData = dtpData2.Value - dtpData1.Value;
            double dias = SomaData.TotalDays;
            decimal dia = Convert.ToInt32(dias);          

            decimal passagem = dto.ValorPassagemArea;
            decimal adulto = dto.ValorHotelAdulto;
            decimal crianca = dto.ValorHotelCrianca;
            adulto = adulto * Convert.ToInt32(txtAdultos.Text);
            crianca = crianca * Convert.ToInt32(txtCrianca.Text);
            decimal totalestadia = crianca + adulto;

            decimal passagemadulto = passagem * Convert.ToInt32(txtAdultos.Text);
            decimal passagemcrianca = passagem * Convert.ToInt32(txtCrianca.Text);
            decimal totalpassagem = passagemadulto + passagemcrianca;

            decimal soma = totalestadia + totalpassagem;

            

            lblPreco.Text = Convert.ToString(soma); 



        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtAdultos_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
